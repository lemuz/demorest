package com.prueba.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.prueba.entity.Demo;
import com.prueba.service.DemoService;

@RestController
@RequestMapping("demo")
public class DemoController {

	@Autowired
	private DemoService demoService;

	final Logger logger = LoggerFactory.getLogger(DemoController.class);
	
	private ResponseEntity<Demo> createOrUpdate(Demo demo){
		try {
			logger.info(demo.toString());
			Demo d = demoService.createUpdateDemo(demo);
			if(d!=null) {
				logger.info(d.toString());
				return ResponseEntity.ok(d);
			}
			else {
				return ResponseEntity.badRequest().build();
			}
		}catch(Exception e) {
			System.out.println("Error: " + e.getMessage());
			e.printStackTrace();
			logger.info(demo.toString());
			return ResponseEntity.badRequest().build();
		}
	}
	
	@PostMapping("cu")
	public ResponseEntity<Demo> createDemo(@RequestBody Demo demo){
		return createOrUpdate(demo);
	}
	
	@PutMapping("cu")
	public ResponseEntity<Demo> updateDemo(@RequestBody Demo demo){
		return createOrUpdate(demo);
	}
	
	@HystrixCommand(fallbackMethod = "fallBackDemo", groupKey = "fallback", commandKey = "fallback")
	@GetMapping("get/{id}")
	public ResponseEntity<Demo> find(@PathVariable Integer id){
		try {
			Demo d = demoService.getDemo(id);
			if(d!=null) {
				return ResponseEntity.ok(d);
			}else {
				return ResponseEntity.badRequest().build();
			}
		}catch(Exception e) {
			System.out.println("Error: " + e.getMessage());
			e.printStackTrace();
			logger.info(id.toString());
			return ResponseEntity.badRequest().build();
		}
	}
	
	public ResponseEntity<Demo> fallBackDemo(Integer id) {
		Demo d = new Demo(id,"Error");
		return ResponseEntity.ok(d);
	}
	
	@SuppressWarnings("rawtypes")
	@DeleteMapping("del/{id}")
	public ResponseEntity delete(@PathVariable Integer id) {
		try {
			boolean d = demoService.delete(id);
			if(d) {
				return ResponseEntity.ok().build();
			}else {
				return ResponseEntity.badRequest().build();
			}
		}catch(Exception e) {
			System.out.println("Error: " + e.getMessage());
			e.printStackTrace();
			logger.info(id.toString());
			return ResponseEntity.badRequest().build();
		}
	}
}