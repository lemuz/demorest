package com.prueba.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "demo_table")
public class Demo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id_demo_table")
	private Integer idDemo;
	
	@Basic(optional = true)
	@Column(name = "nombre")
	private String nombreDemo;

	public Integer getIdDemo() {
		return idDemo;
	}

	public void setIdDemo(Integer idDemo) {
		this.idDemo = idDemo;
	}

	public String getNombreDemo() {
		return nombreDemo;
	}

	public void setNombreDemo(String nombreDemo) {
		this.nombreDemo = nombreDemo;
	}

	public Demo(Integer idDemo, String nombreDemo) {
		super();
		this.idDemo = idDemo;
		this.nombreDemo = nombreDemo;
	}

	public Demo() {
		super();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idDemo == null) ? 0 : idDemo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Demo other = (Demo) obj;
		if (idDemo == null) {
			if (other.idDemo != null)
				return false;
		} else if (!idDemo.equals(other.idDemo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Demo [idDemo=" + idDemo + ", nombreDemo=" + nombreDemo + "]";
	}


}