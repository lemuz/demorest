package com.prueba.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractConnection {

	private Connection connection;
	private List<Statement> stm;
	private List<ResultSet> rs;
	private List<PreparedStatement> pStm;
	private String mensaje;
	private boolean error;
	
	public AbstractConnection() {
		super();
		this.stm = new ArrayList<Statement>();
		this.rs = new ArrayList<ResultSet>();
		this.pStm = new ArrayList<PreparedStatement>();
	}

	public Connection getConnection() {
		return this.connection;
	}

	public void setConnection(Connection connection) {
		try {
		this.connection = connection;
		}catch(Exception ce) {
			System.out.println("Error: " + ce.getMessage());
			ce.printStackTrace();
		}
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}
	
	
}
