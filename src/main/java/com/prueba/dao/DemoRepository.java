package com.prueba.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prueba.entity.Demo;

@Repository
public interface DemoRepository extends JpaRepository<Demo, Integer>{

}
