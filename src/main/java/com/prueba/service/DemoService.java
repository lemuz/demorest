package com.prueba.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.prueba.dao.DemoRepository;
import com.prueba.entity.Demo;

@Service
public class DemoService {
	
	@Autowired
	private DemoRepository demoRepository;
	
	final Logger logger = LoggerFactory.getLogger(DemoService.class);
	
	public Demo createUpdateDemo(Demo demo) {
		try {
			Demo d = demoRepository.save(demo);
			if(d!=null) {
				return d;
			}else {
				return new Demo();
			}
		}catch(DataAccessException daE) {
			System.out.println("Error: " + daE.getMessage());
			daE.printStackTrace();
			logger.info(demo.toString());
			return null;
		}
	}
	
	public Demo getDemo(Integer id) {
		try {
			Demo d = demoRepository.findById(id).get();
			if(d!=null) {
				return d;
			}else {
				return new Demo();
			}
		}catch(DataAccessException daE) {
			System.out.println("Error: " + daE.getMessage());
			daE.printStackTrace();
			logger.info(id.toString());
			return null;
		}
	}
	
	public boolean delete(Integer id) {
		try {
			demoRepository.deleteById(id);
			if(demoRepository.findById(id).get()==null) {
				return true;
			}else {
				return false;
			}
		}catch(DataAccessException daE) {
			System.out.println("Error: " + daE.getMessage());
			daE.printStackTrace();
			logger.info(id.toString());
			return false;
		}
	}
}